# PackaTrust

Clone the project <br>
`git clone https://gitlab.com/packatrust/contracts.git` <br>
<br>
and change working directory. <br>
`cd contracts/app`

## Install Truffle
You need to have nodejs installed. <br>
`npm install truffle`

## Migrate and Deploy
Migrate and deploy the PackaTrust contract. <br>
`truffle migrate`

## Interact with contract
See [truffle] (https://www.trufflesuite.com/docs/truffle/getting-started/interacting-with-your-contracts documentation.

## Testing
Test all functions from the PackaTrust contract. <br>
`truffle test`

