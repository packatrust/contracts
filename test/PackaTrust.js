const pkt = artifacts.require("PackaTrust");

let packageName = web3.utils.toHex("test_package")

let package = {
  name: packageName,
  location: "/tmp/file",
  version: [1, 0, 0]
}

let hash = web3.utils.keccak256(JSON.stringify(package))


contract("PackaTrust", async accounts => {
  
  // Test package registration.
  it("Should register a package.", async () => {
    // Unlock Accounts
    let unlockedOne = await web3.eth.personal.unlockAccount(accounts[1], "pkt", 300)
    let unlockedTwo = await web3.eth.personal.unlockAccount(accounts[2], "pkt", 300)
    let instance = await pkt.deployed()
    // console.log("hash: ", hash, "\njson: ", web3.utils.hexToAscii(hash))
    let tx = await instance.register(
      package.name,
      package.location,
      [accounts[1], accounts[2]],
      [
        await web3.eth.sign(hash, accounts[1]),
        await web3.eth.sign(hash, accounts[2])
      ]
    )
    // console.log("tx: ", tx)
  });

  // Test packageCount is bigger than 0 after package registration.
  it("Should check if packageCount is bigger than 0", async () => {
    let instance = await pkt.deployed()
    let packageCount = await instance.packageCount.call()
    // console.log(packageCount)
    assert(packageCount > 0, "Package count should be bigger than 0!")
  })
  
  // Test we get all Packages
  it("Should get all packages", async () => {
    let instance = await pkt.deployed()
    let packages = await instance.getAll()
    // console.log(packages)
    assert(packages.length > 0, "Packages should be bigger than 0.")
  });
  
  // Test we get one package
  it("Should get one package", async () => {
    let instance = await pkt.deployed()
    let ethPackage = await instance.getOne(web3.utils.fromAscii("test_package"))
    let longName = web3.utils.rightPad(packageName, ethPackage.name.length - 2)
    // console.log(longName)
    // console.log(ethPackage.name)
    assert(ethPackage.name == longName, "Package mismatch!")
  });

  // Test signatures
  it("Should return true if all signatures corresponds to the validators address", async () => {
    let verifications = 0
    let instance = await pkt.deployed()
    let p = await instance.getOne(web3.utils.fromAscii("test_package"))
    // console.log(hash)
    for (let i = 0; i < p.validators.length; i++) {
      let verified = await web3.eth.accounts.recover(hash, p.signatures[i])
      if (verified == p.validators[i]) {
        verifications++
      }
    }
    assert.equal(verifications, p.validators.length, "Not all signatures were verified!")
    //console.log(verifications)
  })

  // Test transfer ownership of package
  it("Should transfertOwnership of package", async () => {
    let instance = await pkt.deployed()
    let package = await instance.transfertOwnership(packageName, accounts[3])
    assert.equal(package.owner, accounts[3], "Ownership has not been transfered!")
    // console.log(package)
  });

  // Test update package
  it("should update package with a new version and add a new validator.", async () => {
    // Unlock Accounts
    let unlocked = await web3.eth.personal.unlockAccount(accounts[0], "pkt", 300)
    let unlockedOne = await web3.eth.personal.unlockAccount(accounts[1], "pkt", 300)
    let unlockedTwo = await web3.eth.personal.unlockAccount(accounts[2], "pkt", 300)
    let instance = await pkt.deployed()
    return instance.update(
      package.name,
      package.location,
      [accounts[0], accounts[1], accounts[2]],
      [
        await web3.eth.sign(hash, accounts[0]),
        await web3.eth.sign(hash, accounts[1]),
        await web3.eth.sign(hash, accounts[2])
      ],
      [1,0,1],
      false
    )
  })

  // Test adding a signature to the validators list
  it("Should return true when adding a validator signature", async () => {
    let unlocked = await web3.eth.personal.unlockAccount(accounts[0], "pkt", 300)
    let instance = await pkt.deployed()
    let pack = await instance.getOne(web3.utils.fromAscii("test_package"))
    // console.log(pack)
    let packMsg = {
      name: pack.name,
      location: pack.location,
      version: pack.version
    }
    let packHash = web3.utils.keccak256(JSON.stringify(packMsg))
    let signature = await web3.eth.sign(packHash, accounts[0], "pkt")
    let added = await instance.addValidatorSignature(
      pack,
      signature
    )
    assert(added == true, "Invalid signature")
  })
})
