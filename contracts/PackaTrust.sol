// contracts/Packatrust.sol
// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Pausable.sol";
import "@openzeppelin/contracts/utils/ReentrancyGuard.sol";


/** @title Packa Trust a Package Repository Authenticity Validator **/
contract PackaTrust is Ownable, Pausable, ReentrancyGuard {
    /// @dev The contract PackaTrust handles the registration of packages
    /// in the blockchain. It enables the creation of an authenticity hash
    /// signature which is validated by 3rd parties and therefore can be
    /// compared upon installation.

    mapping(bytes32 => Package) packages;
    bytes32[] public repo;
    uint private TTL = 3600 * 24; // time-to-live for the signature request.
    uint public packageCount;

    // CUSTOM STRUCTS
    struct Package {
        address owner; // owner id (eth address mapping).
        bytes32 name;
        string location; // package location (git, svn, etc..) bytes32 representation.
        address[] validators; // array with addresses chosen by the owner has validators.
        bytes[] signatures; // array with bytes32 signatures from the validators.
        uint8[3] version; // mapping to the Version data model.
        uint created; // timestamp of package creation.
        uint updated; // timestamp od last update.
        bool signedStatus; // signatures status.
    }

    // EVENTS
    event RequestSignature(
        bytes32 package,
        address[] validators
    );

    /// @dev Function to check version
    function versionCheck(uint8[3] memory version, Package memory package) private pure {
        require(version[0] >= package.version[0], "Major version should be bigger or equal than previous!");
        if (version[1] <= package.version[1]) {
            require(version[2] > package.version[2], "Patch version should be bigger or equal than previous!");
        } else {
            require(version[1] > package.version[1], "Minor version should be bigger or equal than previous!");
        }
    }

    /// @dev Function to add a validator and its signature to the validators and signatures arrays
    /// Takes as parameters:
    /// - a package (see CUSTOM STRUCTS);
    /// - a bytes representation of the signature.
    /// It returns a package.
    function addValidatorSignature(
        Package memory p, bytes memory _signature
    ) public view returns (bool) {
        if (timeoutCheck(p.updated)) {
            for (uint i = 0; i < p.validators.length; i++) {
                if (p.validators[i] == msg.sender) {
                    p.signatures[i] = _signature;
                    return true;
                }
            }
            return false;
        }
    }

    /// @dev Function to set given package valid upon all signatures done.
    /// Takes a package as parameter and returns a boolean value.
    function setPackageValid(Package memory p) public pure returns (Package memory) {
        if (p.validators.length == p.signatures.length) {
            p.signedStatus = true;
            return p;
        }
    }

    /**
    @dev Function to set a package.
    It takes as parameters:
    the package address;
    the bytes32 package name;
    the string with the package location;
    an array with the addresses of the chosen validators.
    */
    function setPackage(
        address _owner,
        bytes32 _name,
        string memory _location,
        address[] memory  _validators,
        bytes[] memory _signatures,
        uint8[3] memory _version,
        uint _created, 
        uint _updated,
        bool _signedStatus 
    ) internal returns (Package memory) {
        // create a package object
        Package memory p = Package(
            _owner,
            _name,
            _location,
            _validators,
            _signatures,
            _version,
            _created,
            _updated,
            _signedStatus
            );
        packages[_name] = p;
        repo.push(_name);
        return p;
    }

    /// @dev Shortcut function for package registration
    function register(
        bytes32 _name,
        string memory _location,
        address[] memory _validators,
        bytes[] memory _signatures
        ) public whenNotPaused nonReentrant returns (uint) {
        // Check if name already in use in this contract
        for (uint256 i = 0; i < repo.length; i++) {
            require(repo[i] != _name, "Package name already in use, either chose another one or update current.");
        }
        
        // emit signal for validation
        emit RequestSignature(_name, _validators);

        // set Package
        setPackage(
            msg.sender,  _name, _location, _validators, _signatures, [1,0,0], block.timestamp, block.timestamp, false
            );
        // increment package count
        packageCount++;
        return packageCount;
    }

    /// @dev Shortcut function for package update
    function update (
        bytes32 _name,
        string memory _location,
        address[] memory _validators,
        bytes[] memory _signatures,
        uint8[3] memory _version,
        bool _signedStatus
        ) public whenNotPaused nonReentrant {
        // Check if updater is package owner.
        require(packages[_name].owner == msg.sender, "This address does not have ownership of this package!");

        // emit signal for validation
        emit RequestSignature(_name, _validators);

        // We do a simple verdion check
        versionCheck(_version, packages[_name]);

        // check for timeout
        require(
            timeoutCheck(block.timestamp) == true,
            "Timeout for validators signature! Please repost a new update!"
        );

        // we set the package
        setPackage(
            packages[_name].owner,
            packages[_name].name,
            _location,
            _validators,
            _signatures,
            _version,
            packages[_name].created,
            block.timestamp,
            _signedStatus
        );
    }

    /// @dev Transfer ownership
    function transfertPackageOwnership(bytes32 _name, address _newOwner) public whenNotPaused nonReentrant view returns(Package memory) {
        Package memory p = packages[_name];
        // check if the sender is the actual owner before trasnfer ownership
        require(msg.sender == p.owner);
        p.owner = _newOwner;
        return p;
    }

    /// @dev function to check timeout
    /// Takes as parameter a integer representing the last update timestamp
    /// and either returns true or reverts the call disabling signature access. 
    function timeoutCheck(uint _updated) public view returns (bool) {
        if (_updated <= block.timestamp) {
            return true;
        }
        revert();
    }

    /// @dev Function to get an array with all packages.
    /// It doesnt accept any parameters and returns an array of package names.
    function getAll() public view returns (bytes32[] memory) {
        return repo;
    }

    /// @dev Function to get an array of packages filtered.
    /// Takes as paramatere a bytes32 representing the filter to apply
    /// and returns a bytes32 array with the packages names in bytes32.
    function getAllBy(bytes32 filter) public view returns (bytes32[] memory packageList) {
        for (uint i = 0; i < packageCount; i++) {
            if (filter == bytes32("local")) {
                /*
                if (keccak256(abi.encode(packages[repo[i]].location[0:2])) == keccak256(abi.encode("./"))) { // FIXME need to be OS agnostic
                    packageList[i] = repo[i];
                }
                */
            }
            if (filter == bytes32("own")) {
                if (packages[repo[i]].owner == msg.sender) {
                    packageList[i] = repo[i];
                }
            }
            if (filter == bytes32("pending")) {
                if (packages[repo[i]].signedStatus == false) {
                    packageList[i] = repo[i];
                }
            }
        }
        return packageList;
    }


    /// @dev Function to get one package BY ITS NAME from the available packages.
    /// It takes has parameter an ethereum adress and returns:
    /// - the address of the package owner;
    /// - the string with the location of the package;
    /// - a bytes32 signature hash;
    /// - the package version.
    function getOne(bytes32 _name) public view returns (Package memory) {
        return packages[_name];
    }

    /**
    @dev Function to destroy the contract 
    */
    function destroyMe() public onlyOwner {
        selfdestruct();
    }

     /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused.
     */
    function _pause() public override onlyOwner whenNotPaused {
        _paused = true;
        emit Paused(_msgSender());
    }

    /**
     * @dev Returns to normal state.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function _unpause() public override onlyOwner whenPaused {
        _paused = false;
        emit Unpaused(_msgSender());
    }
}